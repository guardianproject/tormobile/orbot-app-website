# frozen_string_literal: true

# Set missing strings from the main language
Jekyll::Hooks.register :site, :post_read, priority: :high do |site|
  unless site.config['plugins'].include? 'jekyll-locales'
    Jekyll.logger.warn 'Translations:', 'Enable jekyll-locales plugin to set default translation strings'
    next
  end

  site.locales.each do |locale|
    next if locale == site.default_locale

    site.data[locale] = Jekyll::Utils.deep_merge_hashes site.data[site.default_locale], site.data[locale]
  end
end
